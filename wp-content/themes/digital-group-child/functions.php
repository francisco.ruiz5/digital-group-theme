<?php

add_action( 'wp_enqueue_scripts', 'digital_group_enqueue_styles' );
function digital_group_enqueue_styles() {
    wp_enqueue_style( 'bootstrapgrid', get_stylesheet_directory_uri() . '/src/sass/assets/bootstrap4/bootstrap-grid.css' );
    wp_enqueue_style( 'bootstrapreboot', get_stylesheet_directory_uri() . '/src/sass/assets/bootstrap4/bootstrap-reboot.css' );
    wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/src/sass/assets/bootstrap4/bootstrap.css' );
    wp_enqueue_style( 'themecss', get_stylesheet_directory_uri() . '/src/sass/theme.css' );
    wp_enqueue_style( 'editorcss', get_stylesheet_directory_uri() . '/src/sass/custom-editor-style.css' );
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style')
    );
} 

add_filter('use_block_editor_for_post', '__return_false', 10);